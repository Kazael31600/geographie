#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import math
import json
import datetime
import os


#setting location of POI files
script_filename=os.path.abspath(__file__)
pos = script_filename.rfind(os.sep) + 1
script_filename = os.path.abspath(__file__)
path = script_filename[0:pos]
PATH_JSON= os.path.join(path, '../POI/')
PATH_JSON=os.path.normpath(PATH_JSON)

#Functions related to distance computation
###############################################################

def distance(origin, destination):
    """
    computes distance between two tuplets of (lat,long)
    """
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d


#function related to getting POIs based on keywords and distance
###############################################################
def get_pois_from_jsons(keyword,latitutde,longitude,radius=50):
    """
    get all pois from the four json files
    Inputs:
    -keyword: keyword to look for (e.g.  foret)
    -latitude, longitude: lat and long in float
    -radius: radius to search in km
    This is the function to call
    """
    dict_out={}
    l1=get_pois(PATH_JSON+"fma.data.json","fma",keyword,latitutde,longitude,radius)
    l2=get_pois(PATH_JSON+"tour.data.json","tour",keyword,latitutde,longitude,radius)
    l3=get_pois(PATH_JSON+"product.data.json","product",keyword,latitutde,longitude,radius)
    l4=get_pois(PATH_JSON+"place.data.json","place",keyword,latitutde,longitude,radius)
    for l in l1:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l2:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l3:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    for l in l4:
        dict_out[l[0]]={'nom':l[0],'dist':l[1],'keywords':l[2],'description':l[3],'ville':l[4],'code_postal':l[5],'adresse':l[6],'Periodes_regroupees':l[7],'latitude':l[8],'longitude':l[9],'dt':l[10]}
    return(dict_out)
def get_pois(json_file,key,keyword,latitude,longitude,radius=50):
    """
    get all pois with keyword within radius in km
    Inputs: 
    -json-file:input json-file
    -key: keyword to look for in the dict containing the POIs
    -keyword: keyword we are lookingfor (e.g. foret, lac)
    -lat: latitude (e.g. 43.5)
    -longitude: longitude:
    This function is called within get_pois_from_jsons
    """
    t0=datetime.datetime.now()

    lout=[]
    
    with open(json_file) as json_file:
        data = json.load(json_file)
        for p in data['POI'][key]:
            latitude_poi=data['POI'][key][p]['latitude']
            longitude_poi=data['POI'][key][p]['longitude']
            keywords=data['POI'][key][p]['keywords']
            description=data['POI'][key][p]['description']
            ville=data['POI'][key][p]['ville']
            code_postal=data['POI'][key][p]['code_postal']
            adresse=data['POI'][key][p]['adresse']
            try:
                Periodes_regroupees=data['POI'][key][p]['Periodes_regroupees']
            except:
                Periodes_regroupees="x"
            dist=distance((latitude_poi,longitude_poi),(latitude,longitude))
            found=0
            for k in keywords:
                if k.find(keyword)>=0:
                    found+=1
                    t=datetime.datetime.now()
                    dt=t-t0
            if dist<radius and found>0:
                lout.append([p,dist,keywords,description,ville,code_postal,adresse,Periodes_regroupees,latitude_poi,longitude_poi,dt])
    return(lout)




#test functions
###############################################################

def test():

    d=get_pois_from_jsons("forêt",43.498596,1.310822,radius=50)

    for k in d:
        print(k,d[k]['nom'],d[k]['dt'])


