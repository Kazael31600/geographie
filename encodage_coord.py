import geohash
import distances
import proximityhash
INFINI=99999999


#TBD : https://pypi.org/project/suffix-tree/ for data structures

class Area_Geo():
    """
    class to store the area decoded for a key

    """
    def __init__(self,latitude,longitude,error_latitude,error_longitude):
        self.latitude=latitude
        self.longitude=longitude
        self.error_latitude=error_latitude
        self.error_longitude=error_longitude
        self.latitude_min=self.latitude-self.error_latitude
        self.latitude_max=self.latitude+self.error_latitude
        self.longitude_min=self.longitude-self.error_longitude
        self.longitude_max=self.longitude+self.error_longitude

        self.d1=distances.distance((self.latitude_min,self.longitude),(self.latitude_max,self.longitude))
        self.d2=distances.distance((self.latitude,self.longitude_min),(self.latitude,self.longitude_max))
    def display(self):
        print(self.latitude,self.longitude,self.error_latitude,self.error_longitude,self.d1,self.d2)

def encode(latitude,longitude,p):
    """
    encode a lat long. 
    Inputs:
    -latitude
    -longitude
    -p: accuracy ision: number of characters of the key
    """
    return(geohash.encode(latitude, longitude,precision=p))

def decode(clef):
    """
    decode a key towards a lat, a long, and accuracies

    Returns an Area
    """
    sortie=geohash.decode_exactly(clef)
    latitude_sortie=sortie[0]
    longitude_sortie=sortie[1]
    error_latitude=sortie[2]
    error_longitude=sortie[3]
    A=Area_Geo(latitude_sortie,longitude_sortie,error_latitude,error_longitude)
    return(A)

def get_ranges(latitude,longitude,range_min=1,range_max=20):
    """
    computes ranges of the Area given a key length
    """
    dico_out={}
    for k in range(range_min,range_max):
        print(k)
        c=encode(latitude,longitude,k)
        Area=decode(c)
        dico_out[k]=( Area.d1+Area.d2)/2
    return(dico_out)
def get_accuracy(desired_range,latitude,longitude):

    """
    Given a desired_range (eg 50km) and a dict with the ranges available at a given lat long, returns the accuracy to take on the geohash key. 
    It returns the key so that the range desired is smaller than an available range.

    """

    dico_ranges=get_ranges(latitude,longitude)
    valeur_min=INFINI
    accuracy_out=1
    for k in dico_ranges:
        d=dico_ranges[k]
        if desired_range<d:
            l=abs(desired_range-d)
            if l<valeur_min:
                accuracy_out=k
    return(accuracy_out)

def return_hashes_in_radius(latitude,longitude,radius,accuracy,georaptor=True,min_level=1,max_level=7):
    """
    returns all keys in a given radius in km, given a given accuracy. accuracy can be obtained throgh :
    a=get_accuracy(<needed range eg 2km>,latitude,longitude)
    """
    out=proximityhash.create_geohash(latitude,longitude,radius*1000,accuracy,georaptor,min_level,max_level).split(',')
    return(out)


#Tests functions
##############################
def test():
    latitude=43.51
    longitude=1.32



    a=get_accuracy(2,latitude,longitude)
    print("accuracy needed for 2 kms=",a)

    c=return_hashes_in_radius(latitude,longitude,100,a-1,True)
    for t in c:
        print(t,encode(latitude,longitude,20))
        A=decode(t)
        A.display()


         
test()