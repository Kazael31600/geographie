
#!/usr/bin/env python3
import subprocess
import time


PATH_LOCAL_VALHALLA="/home/chabert/Projets/Install/valhalla/" #should not be useful as valhalla will likely be launched through docker

#useful functions
###############################################################

def launch_command(c):
    """
    Launch external command.
    xc: c='ls -ltr'
    """
    out=subprocess.Popen(c, shell=True, stdout=subprocess.PIPE).stdout.read()
    return(out)


def decode(s):
    """
    decodes b string
    """
    out=s.decode('utf-8')
    return(out)

#launching/stopping valhalla server
###############################################################

def launch_server(service,update_docker_image=False):
    """
    launching valhalla server
    Inputs:
    -service is either
        -valhalla service installed locally, called as VALHALLA
        -the valhalla container. Calling service="" or whatever
    -update_docker_image: True if pulling image is need, false otherwise to use existing image
    """
    if service=="VALHALLA":
        try:
            c1="valhalla_service"
            a1=PATH_LOCAL_VALHALLA+"valhalla.json 1 &"
            r=launch_command(c1+" "+a1)
        except:
            pass
    else:
        commande="docker stop valhalla_container"
        r=launch_command(commande)
        
        commande="docker rm -f valhalla_container"
        r=launch_command(commande)

        if update_docker_image:
            commande="docker image rm -f monpseudoestpris/valhalla:latest"
            print(commande)
            r=launch_command(commande)



            commande="docker pull monpseudoestpris/valhalla"
            print(commande)
            r=launch_command(commande)

        

        commande="docker run -d --name valhalla_container monpseudoestpris/valhalla sh -c 'valhalla_service valhalla.json 1' "
        r=launch_command(commande)


def stop_service(service):
    """
    stoping valhalla service
    Input:
    -service: can be either VALHALLA to stop local valhalla service, or container_name to stop docker container
    """
    if service=="VALHALLA":
        pass
    else:
        try:
            nom_container=service
            commande="docker stop "+nom_container
            r=launch_command(commande)
        except:
            pass