#!/usr/bin/env python3

import subprocess
import os
import json
import datetime
import random
import time
import shapely
from shapely.geometry import Point, Polygon
import requests
import valhalla



#useful functons
###############################################################

def launch_command(c):
    """
    Launch external command.
    xc: c='ls' and a='-ltr'
    """
    out=subprocess.Popen(c, shell=True, stdout=subprocess.PIPE).stdout.read()
    return(out)




def launch_server():
    """
    launching valhalla server
    """
    try:
        valhalla.launch_server("",update_docker_image=False)
    except:
        pass

#Managing adresses
###############################################################
class Adresse:
    """
    Class linking an adress and lat/long
    _input can be an adress, or a [lat,long]
    adresse stores the adress, and latlong the lat.lon
    default API is the datagouv API

    """
    def __init__(self,_input):
        self.adresse=''
        self.latlon=[0,0]
        self.typein=type(_input)

        if self.typein==str:
            self.adresse=_input
            self.getLatLong()
        if self.typein==list:
            self.latlon=_input
            print(self.latlon)
            self.getAdressFromLatLong(self.latlon[0],self.latlon[1])


    def getLatLong(self,API="datagouv"):
        """
        gets lat and long from an adress
        """
        if API=="datagouv":
            url='https://api-adresse.data.gouv.fr/search/?q='+self.adresse.replace(',','').replace(" ","+")+''
            
            response = requests.get(url)
            collection = response.json()
            try:
                self.latlon=collection['features'][0]['geometry']['coordinates']
            except:
                self.latlon=[-999999,-999999]
    def getAdressFromLatLong(self,latitude,longitude,API="datagouv"):
        """
        gets adress from a lat long
        """
        if API=="datagouv":
            url='https://api-adresse.data.gouv.fr/reverse/?lon='+(str)(longitude)+'&lat='+(str)(latitude)

            response = requests.get(url)
            collection = response.json()
            try:
                self.adresse=collection['features'][0]['properties']['label']
            except:
                self.adresse="xxxxxx"
    







#routing functions
###############################################################
def return_isochrone(lat_in,long_in,time,how,valhalla_service_type="DOCKER"):
    """
    computation of isochrone surface.
    Inputs:
    -lat_int: latitude of the center
    -long_in: longitude of the center
    -time: max reach time for the isochrone in mn. note: the max value of this parameter is to be set in valhalla.json in the docker container, default value is 120 mn
    -how: method do move: auto, bicycle, pedestrian, or multimodal
    """
    c=" curl --silent "

    request={"locations":[{"lat":lat_in,"lon":long_in}],"costing":how,"contours":[{"time":time,"color":"ff0000"}]}
    json_request=(json.dumps(request))
    
    if valhalla_service_type=="DOCKER":
        valhalla_adress="172.17.0.2:8002"
    else:
        valhalla_adress="localhost:8002"

    a= " "+valhalla_adress+"/isochrone --data '"+json_request+" '"
    print(c+" "+ a)
    r=(launch_command(c+" "+a))
    d = json.loads(r)
    return(d)

def return_time_between_two_latlong(lat_in,long_in,lat_out,long_out,how,valhalla_service_type="DOCKER"):
    """
    computation of route.
    Inputs:
    -lat_int: latitude of the center
    -long_in: longitude of the center
    -how: method do move: auto, bicycle, pedestrian, or multimodal
    Returns time for the route
    """
    c="curl --silent "

    request={"locations":[{"lat":lat_in,"lon":long_in,"type":"break","city":"Start","state":"France"},{"lat":lat_out,"lon":long_out,"type":"break","city":"Arrival","state":"Paris"}],"costing":how}
    json_request=(json.dumps(request))
    if valhalla_service_type=="DOCKER":
        valhalla_adress="172.17.0.2:8002"
    else:
        valhalla_adress="localhost:8002"
        
    a= " "+valhalla_adress+"/route --data '"+json_request+" '"
    r=(launch_command(c+" "+a))
    d = json.loads(r)
    out=d['trip']['summary']['time']
    return(out)

def is_point_within(point,polygon):
    """
    returns whether a point is within a polygon or not

    """
    is_inside=point.within(polygon)
    return(is_inside)

# TESTS
###############################################################
def test():
    t0=datetime.datetime.today()
    temps_total=0
    for i in range(1,100):
        deltax=random.random()*0.3-0.15
        deltay=random.random()*0.3-0.15
        d=return_time_between_two_latlong(43.5,2,43.5+deltax,2+deltay,"auto")
        print(d)
        temps=(float)(d)
        temps_total = temps_total+temps
        
        t1=datetime.datetime.today()
    temps_total=temps_total/100
    delta_route=t1-t0
    print("100 routes",delta_route,'for an average time of route',temps_total)
    t0=datetime.datetime.today()
    for i in range(100):
        t=random.randint(1,5)
        d=return_isochrone(43.5,2,t,"pedestrian")
        isochrone=d['features'][0]['geometry']['coordinates'] 
        p1 = Point(2.0,43.5)
        polygon=Polygon(isochrone)
        is_inside=is_point_within(p1,polygon)
        print("inside",is_inside)
# Create a Polygon

        print(isochrone )
        t1=datetime.datetime.today()
        print("t=",t1-t0,"for",t)
    delta_iso=t1-t0
    print("100 iso",delta_iso)

    rr=Adresse("12 Impasse Albert Camus 31600 Seysses")
    rr.getLatLong()
    print(rr.latlon)

    rr.getAdressFromLatLong(43.493894,1.30732)
    print(rr.adresse)
    

